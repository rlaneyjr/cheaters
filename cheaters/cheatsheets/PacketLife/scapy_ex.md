<div class="line-gutter-backdrop">

</div>

  ------------------------------------ ------------------------------------
                                       <span
                                       class="html-tag">&lt;html&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;head&gt;</span>

                                       <span
                                       class="html-tag">&lt;title&gt;</span
                                       >Use
                                       Scapy to build your own tools<span
                                       class="html-tag">&lt;/title&gt;</spa
                                       n>

                                       <span class="html-tag">&lt;link
                                       <span
                                       class="html-attribute-name">rel</spa
                                       n>="<span
                                       class="html-attribute-value">stylesh
                                       eet</span>"
                                       <span
                                       class="html-attribute-name">href</sp
                                       an>="[scapy.css](http://www.secdev.o
                                       rg/projects/scapy/scapy.css){.html-a
                                       ttribute-value
                                       .html-resource-link}" <span
                                       class="html-attribute-name">type</sp
                                       an>="<span
                                       class="html-attribute-value">text/cs
                                       s</span>"&gt;</span>

                                       <span
                                       class="html-tag">&lt;/head&gt;</span
                                       >

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/body&gt;</span
                                       >

                                       <span
                                       class="html-tag">&lt;h1&gt;</span>Us
                                       e
                                       Scapy to build your own tools<span
                                       class="html-tag">&lt;/h1&gt;</span>

                                       \
                                       

                                       \
                                       

                                       You can use <span
                                       class="html-tag">&lt;a <span
                                       class="html-attribute-name">href</sp
                                       an>="[index.html](http://www.secdev.
                                       org/projects/scapy/index.html){.html
                                       -attribute-value
                                       .html-external-link}"&gt;</span>Scap
                                       y<span
                                       class="html-tag">&lt;/a&gt;</span>
                                       to make your own automated tools.
                                       You can also extend Scapy without

                                       having to edit its source file.

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       If you have built some interesting
                                       tools, please contribute back to the
                                       mailing-list!

                                       \
                                       

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;h2&gt;</span>Ex
                                       tending
                                       Scapy<span
                                       class="html-tag">&lt;/h2&gt;</span>

                                       \
                                       

                                       If you need to add some new
                                       protocols, new functions, anything,
                                       you can write it directly

                                       into Scapy source file. But this is
                                       not very conveniant. Even if those
                                       modifications are to be

                                       integrated into Scapy, it can be
                                       more conveniant to write them in a
                                       separate file.

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       Once you've done that, you can
                                       launch Scapy and import your file,
                                       but this is still not

                                       very conveniant. Another way to do
                                       that is to make your file executable
                                       and have it call

                                       the Scapy function named <span
                                       class="html-tag">&lt;tt&gt;</span>in
                                       teract()<span
                                       class="html-tag">&lt;/tt&gt;</span>.

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;pre&gt;</span>

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#!
                                       /usr/bin/env python

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#
                                       Set log level to benefit from Scapy
                                       warnings

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       logging

                                       logging<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >getLogger<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;scapy&quot;<spa
                                       n
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>).<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >setLevel<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>from<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       scapy.all <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\*

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>class<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       Test<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >Packet<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>):

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       name <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;Test
                                       packet&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       fields\_desc <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=
                                       \[<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       ShortField<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;test1&quot;<spa
                                       n
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>),

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       ShortField<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;test2&quot;<spa
                                       n
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>2<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)
                                       \]

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>def<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       make\_test<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >x<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >y<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>):

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>return<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       Ether<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>()/<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >IP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>()/<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >Test<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >test1<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >x<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >test2<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >y<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>if<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       \_\_name\_\_ <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>==<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\_\_main\_\_&qu
                                       ot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>:

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       interact<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >mydict<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>globals<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(),<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       mybanner<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;Test
                                       add-on v3.14&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span
                                       class="html-tag">&lt;/pre&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       If you put the above listing in the
                                       <span
                                       class="html-tag">&lt;tt&gt;</span>te
                                       st\_interact.py<span
                                       class="html-tag">&lt;/tt&gt;</span>
                                       file and make it executable,

                                       you'll get :

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;pre&gt;</span>

                                       \# ./test\_interact.py

                                       Welcome to Scapy (0.9.17.109beta)

                                       Test add-on v3.14

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">prompt<
                                       /span>&gt;</span>&gt;&gt;&gt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       make\_test(42,666)

                                       <span
                                       class="html-tag">&lt;span&gt;</span>
                                       &lt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">layer\_
                                       name</span>&gt;</span>Ether<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       name</span>&gt;</span>type<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       =<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       value</span>&gt;</span>0x800<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       |<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       &lt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">layer\_
                                       name</span>&gt;</span>IP<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       |<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       &lt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">layer\_
                                       name</span>&gt;</span>Test<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       name</span>&gt;</span>test1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       =<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       value</span>&gt;</span>42<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       name</span>&gt;</span>test2<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       =<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>=<span
                                       class="html-attribute-value">field\_
                                       value</span>&gt;</span>666<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       |<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       &gt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span&gt;</span>
                                       &gt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >&gt;

                                       <span
                                       class="html-tag">&lt;/pre&gt;</span>

                                       \
                                       

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;h2&gt;</span>Us
                                       ing
                                       Scapy in your tools<span
                                       class="html-tag">&lt;/h2&gt;</span>

                                       \
                                       

                                       You can easily use Scapy in your own
                                       tools. Just import what you need

                                       and do it.

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       This first example take an IP or a
                                       name as first parameter, send an
                                       ICMP echo request

                                       packet and display the completely
                                       dissected return packet.

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;pre&gt;</span>

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#!
                                       /usr/bin/env python

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       sys

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>from<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       scapy.all <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       sr1<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >IP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ICMP

                                       \
                                       

                                       p<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >sr1<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >IP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >dst<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >sys<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >argv<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\[<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\])/<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ICMP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>())

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>if<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       p<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>:

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       p<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >show<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>()<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span
                                       class="html-tag">&lt;/pre&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       This is a more complex example which
                                       does an ARP ping and

                                       reports what it found with LaTeX
                                       formating.

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;pre&gt;</span>

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#!
                                       /usr/bin/env python

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#
                                       arping2tex : arpings a network and
                                       outputs a LaTeX table as a result

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       sys

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>if<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>len<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >sys<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >argv<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)
                                       !=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>2<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>:

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;Usage:
                                       arping2tex &lt;net&gt;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">esc</sp
                                       an>"&gt;</span>\\n
                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>eg:
                                       arping2tex 192.168.1.0/24&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       sys<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >exit<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>from<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       scapy.all <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwc</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       srp<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >Ether<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ARP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >conf

                                       conf<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >verb<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>0

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ans<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >unans<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >srp<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >Ether<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >dst<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;ff:ff:ff:ff:ff:
                                       ff&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)/<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ARP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >pdst<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >sys<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >argv<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\[<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\]),

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       timeout<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>2<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\\begin{tabular
                                       }{|l|l|}&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\\hline&quot;<s
                                       pan
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;MAC
                                       &amp; IP\\\\&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\\hline&quot;<s
                                       pan
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>for<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       snd<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >rcv
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>in<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       ans<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>:

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       rcv<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >sprintf<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >r<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;%Ether.src%
                                       &amp; %ARP.psrc%\\\\&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)

                                       <span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\\hline&quot;<s
                                       pan
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>print<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       r<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;\\end{tabular}&
                                       quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span
                                       class="html-tag">&lt;/pre&gt;</span>

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       Here is another tool that will
                                       constantly monitor all interfaces on
                                       a machine and print all ARP request
                                       it sees, even on 802.11 frames from
                                       a Wi-Fi card in monitor mode.

                                       Note the <span
                                       class="html-tag">&lt;tt&gt;</span>st
                                       ore=0<span
                                       class="html-tag">&lt;/tt&gt;</span>
                                       parameter to <span
                                       class="html-tag">&lt;tt&gt;</span>sn
                                       iff()<span
                                       class="html-tag">&lt;/tt&gt;</span>
                                       to avoid storing all packets in
                                       memory for nothing.

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;pre&gt;</span>

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#!
                                       /usr/bin/env python<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>from<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       scapy.all <span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>import<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\*<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       \
                                       

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>def<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwd</sp
                                       an>"&gt;</span>arp\_monitor\_callbac
                                       k<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >pkt<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>):<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>if<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       ARP <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>in<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       pkt <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>and<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       pkt<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\[<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >ARP<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>\].<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >op
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>in<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>1<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>2<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>):<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">slc</sp
                                       an>"&gt;</span>\#who-has
                                       or is-at<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwa</sp
                                       an>"&gt;</span>return<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       pkt<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>.<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwd</sp
                                       an>"&gt;</span>sprintf<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;%ARP.hwsrc%
                                       %ARP.psrc%&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       \
                                       

                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwd</sp
                                       an>"&gt;</span>sniff<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>(<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >prn<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >arp\_monitor\_callback<span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       <span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">kwb</sp
                                       an>"&gt;</span>filter<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">str</sp
                                       an>"&gt;</span>&quot;arp&quot;<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>,<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >
                                       store<span class="html-tag">&lt;span
                                       <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>=<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">num</sp
                                       an>"&gt;</span>0<span
                                       class="html-tag">&lt;/span&gt;</span
                                       ><span
                                       class="html-tag">&lt;span <span
                                       class="html-attribute-name">class</s
                                       pan>="<span
                                       class="html-attribute-value">sym</sp
                                       an>"&gt;</span>)<span
                                       class="html-tag">&lt;/span&gt;</span
                                       >

                                       <span
                                       class="html-tag">&lt;/pre&gt;</span>

                                       \
                                       

                                       \
                                       

                                       \
                                       

                                       \
                                       

                                       <span
                                       class="html-tag">&lt;p&gt;</span>

                                       For a real life example, you can
                                       check <span class="html-tag">&lt;a
                                       <span
                                       class="html-attribute-name">href</sp
                                       an>=<http://sid.rstack.org/index.php
                                       /Wifitap_EN>&gt;</span>Wifitap<span
                                       class="html-tag">&lt;/a&gt;</span>

                                       \
                                       

                                       \
                                       

                                       <span
                                       class="html-end-of-file"></span>
  ------------------------------------ ------------------------------------
